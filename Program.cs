﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace ringba_test
{
    class Program
    {

        static string REMOTE_URL = "https://ringba-test-html.s3-us-west-1.amazonaws.com/TestQuestions/output.txt";

        static string TEMP_FILE = Path.GetTempPath() + "ringba-test.txt";

        static void Main(string[] args)
        {
            try
            {
                if(!File.Exists(TEMP_FILE))
                {
                    Uri uri = new Uri(REMOTE_URL);
                    using (WebClient web_client = new WebClient())
                    {
                        web_client.DownloadFileCompleted += new AsyncCompletedEventHandler(downloadCompleteCallback);
                        web_client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(readProgressCallback);
                        web_client.DownloadFileAsync(uri, TEMP_FILE);
                    }
                }else{
                    Console.WriteLine("The file already exist.");
                    readLinesStream();
                }
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Console.ReadLine();
        }
        
        /**
        * Syncing the percentage of downloading progress.
        */
        static void readProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.WriteLine("{0} downloaded {1} of {2} mb. {3} % complete...", 
                (string)e.UserState, ((e.BytesReceived / 1024f) / 1024f).ToString("#0.##"), ((e.TotalBytesToReceive / 1024f) / 1024f).ToString("#0.##"), e.ProgressPercentage);
        }

        /**
        * Callback function when download finishes.
        */
        static void downloadCompleteCallback(object sender, AsyncCompletedEventArgs e){
            readLinesStream();
        }

        /**
        * Reading the file using stream start to end.
        */
        static void readLinesStream(){
            try
            {   
                using (StreamReader sr = new StreamReader(TEMP_FILE))
                {
                    string line = sr.ReadToEnd();
                    string[] letters = Regex.Split(line, @"(?<=[A-Z])(?=[A-Z][a-z]) | (?<=[^A-Z])(?=[A-Z]) | (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);
                    analyzeStatistics(letters);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /**
         * Analyze and print out the statistics.
         */
        static void analyzeStatistics(string[] letters)
        {
            var common_word = new Dictionary<string, int>();
            var common_prefix = new Dictionary<string, int>();
            // For total count of letters.
            int letter_cnt = letters.Length, cap_cnt = 0;
            foreach (string word in letters)
            {
                // For counting capitalized letter.
                if (word.ToUpper().Equals(word)) cap_cnt++;
                // For most common letter.
                int comm_cnt;
                common_word.TryGetValue(word, out comm_cnt);
                comm_cnt++;
                common_word[word] = comm_cnt;
                //For most common 2char prefix.
                if(word.Length > 2)
                {
                    int pref_cnt;
                    string pref = word.Substring(0, 2);
                    common_prefix.TryGetValue(pref, out pref_cnt);
                    pref_cnt++;
                    common_prefix[pref] = pref_cnt;
                }
            }
            Console.WriteLine($"Total of {letter_cnt} letters in the file.");
            Console.WriteLine($"Total of {cap_cnt} capitalized letters in the file.");
            var commW = mostCommonElement(common_word);
            Console.WriteLine($"The most common word is '{commW.Item1}' and the count is {commW.Item2}.");
            var commP = mostCommonElement(common_prefix);
            Console.WriteLine($"The most common 2 character prefix is '{commP.Item1}' and the count is {commP.Item2}.");
        }

        /**
         * Iterate the hashmap and find the most occurrence of the pair.
         */
        static (string, int) mostCommonElement(Dictionary<string,int> dict)
        {
            string comm_w="";int comm_c=0;
            foreach (var pair in dict)
            {
                if (pair.Value > comm_c)
                {
                    comm_c = pair.Value;
                    comm_w = pair.Key;
                }
            }
            return (comm_w, comm_c);
        }
    }
}
